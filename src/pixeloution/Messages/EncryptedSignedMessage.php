<?php
namespace pixeloution\Messages;
 
use pixeloution\Messages\Signers\HMACSigner;
use pixeloution\Messages\Signers\HMACSignerInterface;
use pixeloution\Messages\Message\MessageInterface;
use pixeloution\Messages\Message\Message;
use pixeloution\Messages\Encryption\EncryptionInterface;
use pixeloution\Messages\Encryption\Encrypter;

/**
 *
 * @author  Erik Wurzer <erik@pixeloution.com>
 */
class EncryptedSignedMessage
{
   # these could use some documentation
   protected $key;
   protected $sig_key;
   protected $encrypter;
   protected $signer;
   

   /**
    * constructor
    *
    * dependencies, dependencies, dependencies
    * 
    * @param [type] $encryption_key [description]
    * @param [type] $signature_key  [description]
    * @param [type] $encrypter      [description]
    * @param [type] $signer         [description]
    *
    * @return object
    */
   public function __construct( 
      $encryption_key,
      $signature_key,
      EncryptionInterface $encrypter = null, 
      HMACSignerInterface $signer    = null )
   {
      $this->key       = $encryption_key;
      $this->sig_key   = $signature_key;
      $this->signer    = $signer    ?: new HMACSigner();
      $this->encrypter = $encrypter ?: new Encrypter();
   }

   /**
    * Encrypt & Sign
    *
    * @param string $message
    * 
    * @return array
    */
   public function prepare( $message )
   {
      $this->encrypter->setKey( $this->key );
      $encrypted = $this->encrypter->encrypt( $message );

      $this->signer->setKey( $this->sig_key );
      $signature = $this->signer->sign( $encrypted );

      return array(
         'message'   => $encrypted
      ,  'signature' => $signature
      );
   }

   /**
    * [validate description]
    * 
    * @param  [type] $message   [description]
    * @param  [type] $signature [description]
    * 
    * @return mixed
    * returns false if the encrypted message is invalid, or the original message
    */
   public function parse( $message, $signature )
   {
      $this->signer->setKey( $this->sig_key );
      
      # if there's not a valid signature, there's a real problem
      if( ! $this->signer->validate($message, $signature) )
      {
         throw new Exception( 'Invalid Signature: Message may have been tampered with.' );
      }


      $this->encrypter->setKey( $this->key );
      return $this->encrypter->decrypt( $message );
   }

}
