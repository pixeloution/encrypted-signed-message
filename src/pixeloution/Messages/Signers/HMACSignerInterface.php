<?php
namespace pixeloution\Messages\Signers;

interface HMACSignerInterface
{
   /**
    * [set_key description]
    * @param [type] $key [description]
    * @return void
    */
   public function setKey( $key );

   /**
    * [sign description]
    * @param  [type] $string [description]
    * @return string
    */
   public function sign( $string );

   /**
    * [validate description]
    * @param  [type] $string       [description]
    * @param  [type] $hashedstring [description]
    * @return bool
    */
   public function validate( $string, $hashedstring );
}