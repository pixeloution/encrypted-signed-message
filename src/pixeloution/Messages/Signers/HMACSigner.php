<?php
namespace pixeloution\Messages\Signers;

class HMACSigner implements HMACSignerInterface
{
   /**
    * this means to, yes, output the hex values, not raw binary data 
    */
   const OUTPUT_LC_HEXITS = false;

   # encryption key, only setable once on an object
   protected $key = null;

   /**
    * [set_key description]
    * @param [type] $key [description]
    * @return void
    */
   public function setKey( $key )
   {
      $this->key = $key;
   }

   /**
    * [sign description]
    * @param  [type] $string [description]
    * @return string
    */
   public function sign( $string )
   {
      return $this->_calcuate_hmac( $string );
   }

   /**
    * [validate description]
    * @param  [type] $string       [description]
    * @param  [type] $hashedstring [description]
    * @return bool
    */
   public function validate( $string, $signature )
   {
      return ( BOOL ) ( $signature == $this->_calcuate_hmac($string) );
   }

   /**
    * [_calcuate_hmac description]
    * @param  [type] $message [description]
    * @param  [type] $secret  [description]
    * @return string
    */
   protected function _calcuate_hmac( $message )
   {  
      if( $this->key == null )
         throw new Exception( 'Can not sign a message without a key/secret' );

      return hash_hmac( 'sha1', $message, $this->key, self::OUTPUT_LC_HEXITS);
   }      
}