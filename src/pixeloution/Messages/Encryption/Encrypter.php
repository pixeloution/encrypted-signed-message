<?php
namespace pixeloution\Messages\Encryption;


class Encrypter implements EncrypterInterface 
{
   protected $key;
   protected $block_size = 128;
   protected $iv_size;

   public function __construct()
   {
      mt_srand();
      $this->iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
   }

   public function setKey( $key )
   {
      $this->key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
   }

   /**
    * encrypt a string
    *
    * @param  string $string
    * accepts the string to be encrypted
    * 
    * @return string
    * concatenated $iv . $encrypted
    * 
    */
   public function encrypt( $plaintext )
   {
      $iv        = mcrypt_create_iv( $this->iv_size, MCRYPT_RAND );
      $encrypted = mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $this->key, $plaintext, MCRYPT_MODE_CBC, $iv );
      
      return base64_encode( $iv . $encrypted );
   }

   /**
    * decrypts a string. assumes the intialization vector is prepended to
    * the ciphertext
    * 
    * @param  [type] $cipher [description]
    *
    * @return mixed
    * returns a decoded string, or false
    */
   public function decrypt( $cipher )
   {
      $cipher = base64_decode( $cipher );
      $iv     = substr( $cipher, 0, $this->iv_size );
      $cipher = substr( $cipher, $this->iv_size );

      return mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $this->key, $cipher, MCRYPT_MODE_CBC, $iv );
   }

}