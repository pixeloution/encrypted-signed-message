<?php
namespace pixeloution\Messages\Encryption;

interface EncrypterInterface
{
   public function setKey( $key );
   public function encrypt( $string );
   public function decrypt( $string );
}
